/***
Simple slide creation
Lance Bachmeier 08/26/2020
License: GPL 2 or later

This program is used to create the slides I've been using in my screencasts.
The goal is to be very simple while offering the functionality that is not
available in other simple slide creators I tried.

To build:

dmd slides.d
ldmd2 slides.d

To use:

./slides file.txt

file.txt is a text file with the information needed to build html slides. Each new component
is revealed by pressing the down arrow. You can undo the reveal by pressing the up arrow. Pressing
the up arrow on the first slide opens the last slide. You can add an optional second argument if
you want to provide the name of the output file. If there's only one argument, it recycles the
name with a .html extension.

Individual slides are created using the usual Pandoc markdown syntax, including equations. That
can be changed to any other format supported by Pandoc by changing the call in the toHtml method
on the Slide struct from -f markdown to -f [format].

A new slide is created with a line containing

+slide

The slide ends with a line containing only

+++

This is one slide:

+slide
# Foo

This is:

- a slide
- information
- gravity
+++

To reveal information in pieces, first create the slide:

+slide
slide info...
+++

Then add to it:

+add
More info
+++

The same slide will be used until encountering +slide to start a new slide.

The important feature that I wanted but couldn't find in other tools is the
ability to edit slides. For instance, suppose I want talk about different parts
of a slide. I can do this:

+slide
Mark's equation
+add

v = b + s + q
+++

Because there was no +++ before +add, it creates the equivalent of

+slide
Mark's equation

v = b + s + q
+++

The reason to insert +add is so that I can edit the equation as I change it. I can do
this:

+slide
Mark's equation

+add
v = b + s + q
+++
+edit
v = **b** + s + q

The number of boys
+++
+edit
v = b + **s** + q

The number of snakes
+++
+edit
v = b + s + **q**

quantity - remember to take the ratio!
+++

That's it. Incremental slides, but a convenient way to replace content to highlight
or explain things. You can edit the css as desired. A couple things I do is use
\textcolor{blue}{...}
in equations to highlight individual parts in blue, and <span class="red">...</span>
to highlight something in the main text. It's possible that I'll add features to make
slide creation and highlighting easier. For now it works well enough, it's just verbose.
***/

import std.conv, std.exception, std.file, std.path, std.process, std.stdio, std.string;

void main(string[] args) {
	enforce(args.length >= 2, "\n\nNot enough arguments: Proper usage is `slides sourcefile [outputfile]`\n\n\n");
	enforce(std.file.exists(args[1]), "\n\nBad first argument: " ~ args[1] ~ " does not exist\n\n\n");
	string src = args[1];
	string dest;
	if (args.length == 3) {
		dest = args[2];
	} else if (args.length == 2) {
		dest = setExtension(args[1], "html");
	}
	//~ string[] lines = readText(src).replace("\\", "\\\\").split("\n");
	string[] lines = readText(src).split("\n");
	string result;
	Slide slide;
	foreach(line; lines) {
		if (line == "+slide") {
			slide.reset();
		} else if (line == "+++") {
			result ~= slide.create();
		} else if (line == "+add") {
			slide.base ~= slide.last;
			slide.last = "";
			slide.addToBase = false;
		} else if (line == "+edit") {
			slide.last = "";
		} else {
			slide.push(line);
		}
	}
	std.file.write(dest, htmlFile(result, slide.counter));
}

struct Slide {
	int counter = 0;
	string base;
	string last;
	bool addToBase = true;

	string create() {
		string result = `<div id="` ~ counter.to!string ~ `" style="display: none;">` ~ "\n" ~ toHtml(strip(base.stripRight ~ "\n" ~ last.stripRight)) ~ "</slide>\n\n";
		counter += 1;
		return result;
	}
	
	void reset() {
		base = "";
		last = "";
		addToBase = true;
	}
	
	void push(string s) {
		if (addToBase) {
			base ~= s ~ "\n";
		} else {
			last ~= s ~ "\n";
		}
	}

	string toHtml(string s) {
		writeln(s);
		return executeShell("echo '" ~ s.replace(`'`, `'"'"'`) ~ "' | pandoc -t html -f markdown --mathjax").output;
	}
}

string htmlFile(string data, int counter) {
	return `<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="generator" content="pandoc" />
<style type="text/css">
body {
	margin: auto;
	max-width: 820px;
  font-family: Roboto, Arial, sans-serif;
  font-size: 220%;
  padding-bottom: 150px;
}

.content {
    margin: auto;
    font-size: 123%;
    margin-top: 0px;
    margin-bottom: 0px;
    font-family: Roboto, Arial, sans-serif;
    display: flex;
}

a {
	text-decoration: none;
	color: black;
}

h1 {
	font-size: 160%;
	line-height: 1.0em;
  font-family: Ubuntu, "Trebuchet MS", sans-serif;
}

h2 {
  font-family: Ubuntu, "Trebuchet MS", sans-serif;
  font-size: 110%;
	line-height: 0.5em;
	margin-bottom: 0px;
}

code {
  font-family: "Ubuntu Mono", "Courier New", monospace;
  font-size: 110%;
  background: none;
}

pre > code {
  padding: .2rem .5rem;
  margin: 0 .2rem;
  white-space: nowrap;
  border: 1px solid gray;
  border-radius: 1px;
  display: block;
  padding: 1rem 1.5rem;
  white-space: pre-wrap; 
}

pre {
	background: none;
}

.header {
  text-align: right;
  color: brown;
}

blockquote {
  font-family: Lato, Georgia, serif;
  color: brown;
}

blockquote:before {
  border-left: 1px solid gray;
}

.areas {
	flex: 30%;
}

.projects {
	flex: 30%;
}

.notes {
	flex: 40%;
}

.big {
	font-size: 148%;
}

.red {
	color: red;
}
</style>
<script>
MathJax = {
  tex: {
    inlineMath: [['$', '$'], ['\\(', '\\)']]
  },
  svg: {
    fontCache: 'global'
  }
};
</script>
<script type="text/javascript" id="MathJax-script" async
  src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js">
</script>
</head>
<body><div id="thetext"></div>
` ~ data ~ `
<script>
var page = 0;

document.getElementById("thetext").innerHTML = document.getElementById("0").innerHTML;

function nextPage() {
	if (page < ` ~ counter.to!string ~ `-1) {
        console.log(page);
       	page = page+1;
        document.getElementById("thetext").innerHTML = document.getElementById("" + page + "").innerHTML;
        console.log(page);
    }
}

function previousPage() {
	if (page > 0) {
    	if (page >= ` ~ counter.to!string ~ `) {
        	page = page-1;
        } 
        console.log(page);
    	document.getElementById("thetext").innerHTML = document.getElementById("" + page-1 + "").innerHTML;
        page = page-1;
        console.log(page);
    } else if (page == 0) {
		page = ` ~ counter.to!string ~ `+1;
		document.getElementById("thetext").innerHTML = document.getElementById("" + ` ~ counter.to!string ~ `+1 + "").innerHTML;
	}
}

addEventListener("keydown", function(event) {
	if (event.keyCode == 40) {
		nextPage();
	} else if (event.keyCode == 38) {
    	previousPage();
    }
})
</script>

</body>
</html>
`;
}

